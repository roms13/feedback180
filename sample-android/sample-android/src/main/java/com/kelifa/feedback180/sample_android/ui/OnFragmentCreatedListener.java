package com.kelifa.feedback180.sample_android.ui;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

/**
 * Created by Romain Kelifa on 24/05/2016.
 */
public interface OnFragmentCreatedListener {
    void onFragmentCreated( @Nullable Fragment fragment );
}
