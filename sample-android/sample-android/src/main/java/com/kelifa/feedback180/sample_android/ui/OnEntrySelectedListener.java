package com.kelifa.feedback180.sample_android.ui;

/**
 * Created by Romain Kelifa on 26/05/2016.
 */
public interface OnEntrySelectedListener {
    void onEntrySelected (int position);
}
