package com.kelifa.feedback180.sample_android.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kelifa.feedback180.sample_android.R;
import com.kelifa.feedback180.sample_android.dto.Entry;
import com.squareup.picasso.Picasso;

import java.net.URL;

/**
 * Created by Romain Kelifa on 17/05/2016.
 */
public class EntryAdapter extends BaseAdapter {

    private Context context;
    private int resource;
    private LayoutInflater inflater;
    private Entry[] entries;

    public EntryAdapter(Context context, int resource, Entry[] entries) {
        this.entries = entries;
        this.context = context;
        this.resource = resource;
        this.inflater = LayoutInflater.from( context );
    }

    @Override
    public int getCount() {
        return entries != null ? entries.length : -1;
    }

    @Override
    public Object getItem(int position) {
        return entries != null && entries.length >= position ? entries[position] : null;
    }

    @Override
    public long getItemId(int position) {
        int count = getCount();
        if ( count >= position )
        {
            return entries[position].getId();
        }

        return -1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //(1) : Réutilisation des layouts
        if (convertView == null) {
            //Initialisation de notre item à partir du  layout XML "personne_layout.xml"
            convertView = (LinearLayout) inflater.inflate(R.layout.adapter_entry, parent, false);
        }

        /* Extract the entry's object to show */
        Entry entry = (Entry) getItem( position );

        /* Take the TextView from layout and set the entry's name */
        TextView txtName = (TextView) convertView.findViewById(R.id.rendererTitleTextView);
        txtName.setText(entry.getName());

        TextView txtDetail = (TextView) convertView.findViewById(R.id.rendererDetailTextView);
        txtDetail.setText(entry.getDetail());

        /* Take the ImageView from layout and set the entry's image */
        ImageView thumbView = (ImageView) convertView.findViewById(R.id.rendererImageView);
        try {
            URL uri = new URL(entry.getPic_thumb());
            Picasso.with(context)
                    .load(uri.toString())
                    .placeholder( R.drawable.an_loading )
                    .into(thumbView);
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return convertView;
    }
}
