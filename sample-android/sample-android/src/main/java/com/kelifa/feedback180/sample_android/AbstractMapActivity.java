package com.kelifa.feedback180.sample_android;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.kelifa.feedback180.sample_android.dto.Entry;
import com.kelifa.feedback180.sample_android.ui.DetailFragment;
import com.kelifa.feedback180.sample_android.ui.LocationFragment;

import java.util.HashMap;

/**
 * Created by Romain Kelifa on 26/05/2016.
 */
public class AbstractMapActivity extends AbstractActivity
        implements OnMapReadyCallback, GoogleMap.OnInfoWindowClickListener
{
    protected GoogleMap map;
    protected HashMap<Marker,Entry> hashmap;

    boolean isDisplayingMap;

    @Override
    protected void onPause()
    {
        super.onPause();
        if ( map != null ) map.setOnInfoWindowClickListener(null);
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if ( map != null ) map.setOnInfoWindowClickListener(this);
    }

    @Override
    public void onMapReady(GoogleMap map)
    {
        // Initialize map
        if ( this.map == null )
        {
            this.map = map;
            if ( map != null )
            {
                map.setOnInfoWindowClickListener(this);
                UiSettings settings = this.map.getUiSettings();
                settings.setMyLocationButtonEnabled(false);
                settings.setCompassEnabled(false);
                settings.setZoomControlsEnabled(true);
                settings.setZoomGesturesEnabled(true);
                LatLng latLng = new LatLng( 13.74343, 100.53502 );
                map.moveCamera( CameraUpdateFactory.newLatLngZoom( latLng, 12.0f ) );
            }
        }

        refreshMarkers();
    }

    @Override
    public void onInfoWindowClick(Marker marker)
    {
        Entry entry = hashmap != null ? hashmap.get(marker) : null;

        if ( isLargeScreen() )
        {
            FragmentManager manager = getSupportFragmentManager();
            LocationFragment locationFragment = (LocationFragment) manager.findFragmentByTag(getResources().getString(R.string.tag_map));

            DetailFragment detailFragment = DetailFragment.newInstance(entry);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            if ( locationFragment != null )
            {
                transaction.detach( locationFragment );
            }

            // Add the fragment to the placeholder FrameLayout
            transaction.replace(R.id.fragmentDetailLayout, detailFragment, getResources().getString(R.string.tag_detail));

            transaction.commit();

            setIsDisplayingMap(false);
        }
        else
        {
            if ( entry != null )
            {
                Intent intent = new Intent(this, DetailActivity.class);
                intent.putExtra(DetailFragment.EXTRA_MESSAGE_ENTRY, entry);
                startActivity(intent);
            }
        }
    }

    protected void refreshMarkers ()
    {
        if ( map != null )
        {
            map.clear();
            createMarkers();
        }
    }

    private void createMarkers ()
    {
        MarkerOptions[] options = getMarkersOptions();
        int count = options != null ? options.length : 0;
        int i = 0;
        MarkerOptions option = null;
        Entry entry = null;
        Marker marker = null;

        if ( count > 0 )
        {
            Entry[] entries = model.getEntries();

            hashmap = new HashMap<Marker,Entry>();

            for ( ; i < count ; i++ )
            {
                entry = entries[i];
                option = options[i];

                marker = map.addMarker(option);
                hashmap.put(marker,entry);
            }
        }
    }

    @Nullable
    private MarkerOptions[] getMarkersOptions ()
    {
        Entry[] entries = model.getEntries();
        int count = entries != null ? entries.length : 0;

        if ( count > 0 )
        {
            MarkerOptions[] options = new MarkerOptions[count];
            int i = 0;
            MarkerOptions option = null;
            Entry entry = null;
            LatLng latLng = null;

            for ( ; i < count ; i++ )
            {
                entry = entries[i];
                if ( entry != null )
                {
                    latLng = new LatLng(entry.getLat(),entry.getLng());
                    option = new MarkerOptions()
                            .title(entry.getName())
                            .snippet(entry.getLocation())
                            .position(latLng);
                    options[i] = option;
                }
            }

            return options;
        }

        return null;
    }

    protected void setIsDisplayingMap (Boolean visible )
    {
        if ( isDisplayingMap != visible )
        {
            isDisplayingMap = visible;
            onIsDisplayingMapChanged();
        }
    }

    protected void onIsDisplayingMapChanged ()
    {}
}
