package com.kelifa.feedback180.sample_android.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kelifa.feedback180.sample_android.R;
import com.kelifa.feedback180.sample_android.conf.Conf;
import com.kelifa.feedback180.sample_android.dto.Entry;
import com.kelifa.feedback180.sample_android.util.Localization;

/**
 * Created by Romain Kelifa on 19/05/2016.
 */
public class DetailFragment extends AbstractFragment implements OnPictureChangeListener
{
    final static public String EXTRA_MESSAGE_ENTRY = "com.kelifa.feedback180.sample-android.detail.MESSAGE";
    private TextView nameView;
    private TextView detailView;
    private TextView locationTitleView;
    private TextView locationView;
    private TextView conditionTitleView;
    private TextView conditionView;

    private Entry entry;
    private PictureFragment pictureFragment;
    private CommentFragment commentFragment;
    private View view;

    public static DetailFragment newInstance(Entry entry) {
        DetailFragment fragment = new DetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(EXTRA_MESSAGE_ENTRY, entry);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_detail, container, false);

        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(EXTRA_MESSAGE_ENTRY)) {
                entry = args.getParcelable(EXTRA_MESSAGE_ENTRY);
            }
        }
        return view;
    }

    @Override
    public void createUI()
    {
        pictureFragment = PictureFragment.newInstance(this);
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.fragmentDetailPictureLayout, pictureFragment).commit();

        commentFragment = CommentFragment.newInstance(this,entry);
        transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.fragmentDetailCommentLayout, commentFragment).commit();

        nameView = (TextView) view.findViewById(R.id.nameView);
        detailView = (TextView) view.findViewById(R.id.detailView);
        conditionTitleView = (TextView) view.findViewById(R.id.conditionTitleView);
        conditionView = (TextView) view.findViewById(R.id.conditionView);
        locationTitleView = (TextView) view.findViewById(R.id.locationTitleView);
        locationView = (TextView) view.findViewById(R.id.locationView);
    }

    @Override
    public void updateUI()
    {
        if ( isAdded() )
        {
            if ( commentFragment != null ) commentFragment.updateUI();
            if ( pictureFragment != null ) pictureFragment.updateUI();

            updateTexts();
            updatePicture();
        }
    }

    @Override
    public void onFragmentCreated (Fragment fragment)
    {
        super.onFragmentCreated(fragment);

        if ( fragment == pictureFragment )
        {
            onPictureFragmentCreated();
        }
        else if ( fragment == commentFragment )
        {
            onCommentFragmentCreated();
        }
    }

    @Override
    public void onSuccess()
    {
        Log.d(Conf.PICASSO_TAG,"picture retrieved");
    }

    @Override
    public void onError()
    {
        Log.d(Conf.PICASSO_TAG,"error retrieving picture");
    }

    private void onPictureFragmentCreated ()
    {
        updatePicture();
        refreshTexts();
    }

    private void onCommentFragmentCreated ()
    {
        updateTexts();
        commentFragment.refresh();
    }

    private void refreshTexts()
    {
        String none = getResources().getString(R.string.property_none);
        String name = entry != null ? entry.getName() : none;
        String detail = entry != null ? entry.getDetail() : none;
        String location = entry != null ? entry.getLocation() : none;
        String condition = entry != null ? entry.getCondition() : none;
        nameView.setText( name );
        detailView.setText( detail );
        locationView.setText( location );
        conditionView.setText( condition );
    }

    private void updateTexts()
    {
        if ( isAdded() )
        {
            conditionTitleView = (TextView) view.findViewById(R.id.conditionTitleView);
            locationTitleView = (TextView) view.findViewById(R.id.locationTitleView);

            locationTitleView.setText(getString(R.string.detail_location));
            conditionTitleView.setText(getString(R.string.detail_condition));

            locationTitleView.setTextSize(TypedValue.COMPLEX_UNIT_SP,
                    Localization.isThai( getContext() ) ? Conf.THAI_FONT_SIZE : Conf.STANDARD_FONT_SIZE );
            conditionTitleView.setTextSize(TypedValue.COMPLEX_UNIT_SP,
                    Localization.isThai( getContext() ) ? Conf.THAI_FONT_SIZE : Conf.STANDARD_FONT_SIZE );
        }
    }

    private void updatePicture ()
    {
        if ( isAdded() )
        {
            String pic = entry != null ? entry.getPic() : null;
            pictureFragment.setPath( pic );
        }
    }
}
