package com.kelifa.feedback180.sample_android.provider;

import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.net.Uri;

import com.kelifa.feedback180.sample_android.helper.DatabaseHelper;

/**
 * Created by Romain Kelifa on 23/05/2016.
 */
public class CommentProvider extends ContentProvider {
    // provider name
    public static final String CONTENT_PROVIDER_AUTHORITY = "com.kelifa.feedback180.sample";
    // provider authority uri
    public static final Uri CONTENT_PROVIDER_AUTHORITY_URI = Uri.parse( "content://" + CONTENT_PROVIDER_AUTHORITY );
    // provider uri path
    private static final String CONTENT_PATH = "comment";
    // database URI
    public static final Uri CONTENT_URI = Uri.withAppendedPath( CONTENT_PROVIDER_AUTHORITY_URI, CONTENT_PATH );

    // content provider MIME
    public static final String CONTENT_PROVIDER_MIME = ContentResolver.CURSOR_DIR_BASE_TYPE
            + "/vnd."
            + CONTENT_PROVIDER_AUTHORITY
            + "."
            + CONTENT_PATH;
    public static final String CONTENT_PROVIDER_MIME_ITEM = ContentResolver.CURSOR_ITEM_BASE_TYPE
            + "/vnd."
            + CONTENT_PROVIDER_AUTHORITY
            + "."
            + CONTENT_PATH;

    // content provider URI identifiers
    private static final int COMMENTS = 1;
    private static final int COMMENT_BY_ID = 2;
    private static final int COMMENTS_BY_ENTRY_ID = 3;

    static private UriMatcher matcher;
    private static UriMatcher getUriMatcher() {
        if ( matcher == null )
        {
            matcher = new UriMatcher(UriMatcher.NO_MATCH);
            matcher.addURI(CONTENT_PROVIDER_AUTHORITY, CONTENT_PATH, COMMENTS);
            matcher.addURI(CONTENT_PROVIDER_AUTHORITY, CONTENT_PATH + "/#", COMMENT_BY_ID);
            matcher.addURI(CONTENT_PROVIDER_AUTHORITY, CONTENT_PATH + "/entry/#", COMMENTS_BY_ENTRY_ID);
        }
        return matcher;
    }

    static public DatabaseHelper helper;

    @Override
    public String getType(Uri uri)
    {
        switch (getUriMatcher().match(uri))
        {
            case COMMENT_BY_ID:
                return CONTENT_PROVIDER_MIME_ITEM;
            default: // case COMMENTS && COMMENTS_BY_ENTRY_ID
                return CONTENT_PROVIDER_MIME;
        }
    }

    @Override
    public boolean onCreate()
    {
        helper = new DatabaseHelper(getContext());
        return true;
    }

    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        String id = null;
        Cursor cursor = null;

        switch (getUriMatcher().match(uri))
        {
            case COMMENT_BY_ID:
                id = uri.getLastPathSegment();
                cursor = helper.getCommentById(id,projection,selection,selectionArgs,sortOrder);
                break;
            case COMMENTS_BY_ENTRY_ID:
                id = uri.getLastPathSegment();
                cursor = helper.getCommentsByEntryId(id,projection,selection,selectionArgs,sortOrder);
                break;
            default: // case COMMENTS
                cursor = helper.getComments(projection,selection,selectionArgs,sortOrder);
                break;
        }
        cursor.setNotificationUri(getContext().getContentResolver(),uri);
        return cursor;
    }

    @Override
    public Uri insert(Uri uri, ContentValues values)
    {
        try
        {
            long id = helper.insertComment( values );
            Uri returnUri = ContentUris.withAppendedId(CONTENT_URI,id);
            return returnUri;
        }
        catch (Exception e)
        {
            return null;
        }
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) { return 0; }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) { return 0; }
}
