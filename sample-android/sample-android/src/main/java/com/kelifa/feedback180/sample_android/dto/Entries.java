package com.kelifa.feedback180.sample_android.dto;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

/**
 * Created by Romain Kelifa on 17/05/2016.
 */
public class Entries {
    public static Entry[] fromJson(String s)
    {
        Entry[] decoded = null;
        try
        {
            decoded = new Gson().fromJson(s, Entry[].class);
        }
        catch ( JsonSyntaxException e )
        {
            e.printStackTrace();
        }

        return decoded;
    }
    public String toString() {
        return new Gson().toJson(this);
    }
}
