package com.kelifa.feedback180.sample_android.service;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import java.io.InputStream;

/**
 * Created by Romain Kelifa on 17/05/2016.
 */
public class ThumbTask extends AsyncTask<String, Void, Bitmap> {
    ImageView imageView;

    public ThumbTask(ImageView imageView) {
        this.imageView = imageView;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap thumb = null;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            thumb = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return thumb;
    }

    protected void onPostExecute(Bitmap result) {
        imageView.setImageBitmap(result);
    }
}
