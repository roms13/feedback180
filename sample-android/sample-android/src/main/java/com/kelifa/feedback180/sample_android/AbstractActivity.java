package com.kelifa.feedback180.sample_android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.kelifa.feedback180.sample_android.conf.Conf;
import com.kelifa.feedback180.sample_android.dto.Entry;
import com.kelifa.feedback180.sample_android.model.Model;
import com.kelifa.feedback180.sample_android.service.OnTaskCompletedListener;
import com.kelifa.feedback180.sample_android.ui.Reloadable;
import com.kelifa.feedback180.sample_android.util.Localization;

import java.lang.reflect.Method;
import java.util.Locale;

/**
 * Created by Romain Kelifa on 18/05/2016.
 */
public class AbstractActivity extends AppCompatActivity
        implements OnTaskCompletedListener, Reloadable
{
    final protected Model model = Model.getInstance();

    final static public Locale[] locales = new Locale[] {
            Locale.ENGLISH,
            Localization.LOCALE_THAI,
            Locale.FRENCH
    };

    private Toolbar bar;
    private Locale displayedLocale;

    private MenuItem action_settings_english;
    private MenuItem action_settings_thai;
    private MenuItem action_settings_french;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        extractExtras();
        inflateUI();
        createUI();
        updateUI();
        restorePreference();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        displayedLocale = Locale.getDefault();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        restorePreference();
        updateUI();
    }

    @Override
    protected void onNewIntent(Intent intent)
    {
        super.onNewIntent(intent);
        createUI();
        updateUI();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        action_settings_english = menu.findItem( R.id.action_settings_english );
        action_settings_thai = menu.findItem( R.id.action_settings_thai );
        action_settings_french = menu.findItem( R.id.action_settings_french );

        updateItems();

        return true;
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu)
    {
        if(menu != null){
            if(menu.getClass().getSimpleName().equals("MenuBuilder")){
                try{
                    Method m = menu.getClass().getDeclaredMethod(
                            "setOptionalIconsVisible", Boolean.TYPE);
                    m.setAccessible(true);
                    m.invoke(menu, true);
                }
                catch(NoSuchMethodException e){
                    Log.e(Conf.INITIALIZATION_TAG, "onMenuOpened", e);
                }
                catch(Exception e){
                    throw new RuntimeException(e);
                }
            }
        }
        return super.onMenuOpened(featureId, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();
        Locale locale = null;

        switch ( id )
        {
            case R.id.action_settings_english:
                locale = Locale.ENGLISH;
                break;

            case R.id.action_settings_thai:
                locale = Localization.LOCALE_THAI;
                break;

            case R.id.action_settings_french:
                locale = Locale.FRENCH;
                break;
        }

        if ( locale != null )
        {
            savePreference( locale );
            reload();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted() {
        Log.d(Conf.PROCESSING_TAG,"task complete");
    }

    // to be overriden : use with Intent to avoid missing references
    protected void extractExtras ()
    {}

    // to be overriden : use with setContentView method to avoid NPE
    protected void inflateUI()
    {}

    public void createUI ()
    {
        createBar();
    }

    public void updateUI ()
    {
        updateBar();
        updateItems();
    }

    protected void createBar()
    {
        bar = (Toolbar) findViewById( R.id.action_toolbar );
        if ( bar != null )
        {
            bar.inflateMenu(R.menu.menu_main);
            bar.setTitle("");
            setSupportActionBar(bar);
        }
    }

    private void updateBar()
    {
        if ( bar != null )
        {
            Locale current = getDisplayableLocale();
            bar.setOverflowIcon( ContextCompat.getDrawable( this, getFlag( current ) ) );
        }
    }

    private void updateItems ()
    {
        boolean created = action_settings_english != null
                && action_settings_thai != null
                && action_settings_french != null;

        if ( created )
        {
            Locale current = getDisplayableLocale();
            MenuItem item = null;

            if ( Localization.LOCALE_THAI.equals( current ) )
            {
                item = action_settings_thai;
            }
            else if ( Locale.FRENCH.equals( current ) )
            {
                item = action_settings_french;
            }
            else // default to english
            {
                item = action_settings_english;
            }

            action_settings_english.setVisible( ( item != action_settings_english ) );
            action_settings_thai.setVisible( ( item != action_settings_thai ) );
            action_settings_french.setVisible( ( item != action_settings_french ) );
        }
    }

    protected Boolean hasEntries ()
    {
        Entry[] entries = model.getEntries();
        return ( entries != null ) && ( entries.length > 0 );
    }

    private void savePreference ( Locale locale )
    {
        if ( locale != null && !locale.equals( Localization.getPreferenceLocale( getApplicationContext() ) ) )
        {
            displayedLocale = locale;
            Locale.setDefault( locale );

            SharedPreferences preferences = getSharedPreferences(getString(R.string.preference_language), Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(getString(R.string.preference_language), locale.getLanguage() );
            editor.commit();
        }
    }

    private void restorePreference ()
    {
        Locale preference = Localization.getPreferenceLocale( getApplicationContext() );
        Locale device = Locale.getDefault();

        boolean stored = preference != null;
        boolean defined = device != null;
        boolean modified = displayedLocale != null;
        boolean startup = stored && defined;
        boolean lifecycle = stored && modified;
        boolean different = ( startup && !preference.getLanguage().equals( device.getLanguage() ) )
                || ( lifecycle && !preference.getLanguage().equals( displayedLocale.getLanguage() ) );
        boolean set = stored && !defined;

        if ( different || set ) // during application lifecycle, check for the preference
        {
            Locale.setDefault( preference );
            reload();
        }
        else if ( !stored ) // on initial application launch the preference is undefined so store it
        {
            savePreference( device );
        }
    }

    protected void reload()
    {
        Configuration config = new Configuration();
        config.locale = Localization.getPreferenceLocale( getApplicationContext() );
        Resources resources = getResources();
        resources.updateConfiguration(config, resources.getDisplayMetrics());

        updateUI();
    }

    private int getFlag (Locale locale )
    {
        if ( locale != null )
        {
            if ( Localization.LOCALE_THAI.equals( locale ) )
                return R.drawable.ic_thailand;
            else if ( Locale.FRENCH.equals( locale ) )
                return R.drawable.ic_france;
        }

        // default to english
        return R.drawable.ic_usa;
    }

    private Locale getDisplayableLocale ()
    {
        Locale preference = Localization.getPreferenceLocale( getApplicationContext() );
        if ( preference != null )
            return preference;

        return Locale.getDefault();
    }

    protected boolean isLargeScreen ()
    {
        return ( ( getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK )
                == Configuration.SCREENLAYOUT_SIZE_LARGE );
    }
}
