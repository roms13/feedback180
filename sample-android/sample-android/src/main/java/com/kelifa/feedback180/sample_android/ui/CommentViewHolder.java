package com.kelifa.feedback180.sample_android.ui;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.kelifa.feedback180.sample_android.R;

/**
 * Created by Romain Kelifa on 26/05/2016.
 */
public class CommentViewHolder extends RecyclerView.ViewHolder
{
    public TextView commentDateTextView;
    public TextView commentPostTextView;

    public CommentViewHolder(View itemView)
    {
        super(itemView);
        commentDateTextView = (TextView) itemView.findViewById(R.id.commentDateTextView);
        commentPostTextView = (TextView) itemView.findViewById(R.id.commentPostTextView);
    }
}
