package com.kelifa.feedback180.sample_android.model;

import com.kelifa.feedback180.sample_android.dto.Entry;

/**
 * Created by Romain Kelifa on 18/05/2016.
 */
public class Model
{
    private Entry[] entries;

    public Entry[] getEntries() {
        return entries;
    }

    public void setEntries(Entry[] entries) {
        this.entries = entries;
    }

    private static final Model holder = new Model();
    public static Model getInstance() {return holder;}
}

