package com.kelifa.feedback180.sample_android.ui;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.kelifa.feedback180.sample_android.R;
import com.kelifa.feedback180.sample_android.conf.Conf;
import com.kelifa.feedback180.sample_android.dto.Entries;
import com.kelifa.feedback180.sample_android.dto.Entry;
import com.kelifa.feedback180.sample_android.model.Model;
import com.kelifa.feedback180.sample_android.receiver.WifiReceiver;
import com.kelifa.feedback180.sample_android.service.EntriesTask;
import com.kelifa.feedback180.sample_android.service.OnTaskCompletedListener;

import org.json.JSONException;

/**
 * Created by Romain Kelifa on 17/05/2016.
 */
public class ListFragment extends AbstractFragment implements OnTaskCompletedListener, AdapterView.OnItemClickListener
{
    private View view;
    protected Model model = Model.getInstance();

    final static public String LIST_POSITION = "com.kelifa.feedback180.sample_android.listing.MESSAGE";

    private OnListChangeListener listener;
    private EntriesTask entriesTask;
    private Handler handler;
    private ListView listView;
    private FrameLayout loadingView;
    private LoadingFragment loadingFragment;
    private boolean processing = false;

    @Override public void onAttach(Context context)
    {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try
        {
            listener = (OnListChangeListener) context;
        }
        catch (ClassCastException e)
        {
            throw new ClassCastException(context.toString()
                    + " must implement " + OnListChangeListener.class.getName() );
        }
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_listing, container, false);

        return view; }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        // Check whether we're recreating a previously destroyed instance
        if (savedInstanceState != null) {
            // Restore value of members from saved state
            listView.setSelection( savedInstanceState.getInt(LIST_POSITION) );
        }

        if ( !contains() )
        {
            try
            {
                process();
            }
            catch (JSONException e )
            {
                e.printStackTrace();
            }
        }
        else
        {
            display();
        }
    }

    @Override
    public void createUI()
    {
        loadingFragment = new LoadingFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentLoadingListingLayout, loadingFragment).commit();

        listView = (ListView) view.findViewById(R.id.fragmentListingListView);
        loadingView = (FrameLayout) view.findViewById(R.id.fragmentLoadingListingLayout);
    }

    @Override
    public void updateUI()
    {
        if ( isAdded() )
        {
            if ( loadingFragment != null ) loadingFragment.setNoneMessage( getString( R.string.entries_task_none ) );
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        listView.setOnItemClickListener(null);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        listView.setOnItemClickListener(this);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState)
    {
        // Save the user's current game state
        savedInstanceState.putInt(LIST_POSITION, listView.getFirstVisiblePosition());

        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onTaskCompleted()
    {
        if ( isAdded() )
        {
            Log.d(Conf.DATA_TAG, getResources().getString(R.string.server_response) + ":" + entriesTask.response);
            // display(entriesTask.response); // here Thread error
            Message completeMessage =
                    handler.obtainMessage(200, entriesTask);
            completeMessage.sendToTarget();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        listener.onEntrySelected(position);
    }

    public void cancel ()
    {
        if ( entriesTask != null && processing )
        {
            entriesTask.cancel(true);
            entriesTask.clean();
            processing = false;
        }
    }

    public void resume ()
    {
        if ( !processing )
        {
            try
            {
                process();
            }
            catch (JSONException e )
            {
                e.printStackTrace();
            }
        }
    }

    private void process () throws JSONException
    {
        if ( !processing )
        {
            setProcessing(true);

            Context context = getContext();

            if ( WifiReceiver.hasWifi(context) && WifiReceiver.isConnected(context) )
            {
                if ( handler == null )
                {
                    handler = new Handler(Looper.getMainLooper())
                    {
                        @Override
                        public void handleMessage(Message inputMessage)
                        {
                            EntriesTask s = (EntriesTask) inputMessage.obj;
                            Entry[] retrieved = Entries.fromJson(s.response);
                            update( retrieved );
                            display();
                            setProcessing(false);
                            if ( listener != null ) listener.onTaskCompleted();
                        }
                    };
                }

                entriesTask = new EntriesTask(this);
                entriesTask.execute();
            }
            else
            {
                Log.d(Conf.CONNECTIVITY_TAG, getResources().getString(R.string.connectivity_none));
                processing = false;
            }
        }
    }

    public boolean isProcessing ()
    {
        return processing;
    }

    protected void update ( Entry[] entries ) {
        model.setEntries(entries);
    }

    protected void display ()
    {
        if ( contains() )
        {
            EntryAdapter adapter = new EntryAdapter( getContext(),
                    R.layout.adapter_entry, model.getEntries() );
            listView.setAdapter(adapter);
        }

        updateLoading();
    }

    public boolean contains ()
    {
        Entry[] entries = model != null ? model.getEntries() : null;
        return ( entries != null ) && ( entries.length > 0 );
    }

    private void setProcessing ( boolean processing )
    {
        if ( this.processing != processing )
        {
            this.processing = processing;
            updateLoading();
        }
    }

    private void updateLoading ()
    {
        if ( loadingView != null && loadingFragment != null )
        {
            loadingView.setVisibility( contains() ? View.GONE : View.VISIBLE );
            loadingFragment.setProcessing( processing );
        }
    }
}
