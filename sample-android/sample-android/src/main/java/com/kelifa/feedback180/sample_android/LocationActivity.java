package com.kelifa.feedback180.sample_android;

import android.os.Bundle;

import com.kelifa.feedback180.sample_android.dto.Entry;
import com.kelifa.feedback180.sample_android.ui.LocationFragment;

/**
 * Created by Romain Kelifa on 26/05/2016.
 */
public class LocationActivity extends AbstractMapActivity
{
    private Entry[] entries;
    LocationFragment mapFragment;

    @Override
    protected void inflateUI()
    {
        super.inflateUI();
        setContentView(R.layout.activity_location);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        mapFragment = (LocationFragment) getSupportFragmentManager().findFragmentById(R.id.locationMapFragment);
        mapFragment.getMapAsync(this);
    }
}
