package com.kelifa.feedback180.sample_android.dto;

/**
 * Created by Romain Kelifa on 25/05/2016.
 */
public interface Provided {
    int getId();
}
