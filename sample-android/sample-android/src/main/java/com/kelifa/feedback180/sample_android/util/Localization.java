package com.kelifa.feedback180.sample_android.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.Nullable;

import com.kelifa.feedback180.sample_android.R;

import java.util.Locale;

/**
 * Created by Romain Kelifa on 30/05/2016.
 */
public class Localization
{
    static public Locale LOCALE_THAI = new Locale("th");

    static public boolean isThai ( Context context )
    {
        Locale locale = getPreferenceLocale(context);
        return locale != null && locale.getLanguage().equals( LOCALE_THAI.getLanguage() );
    }

    @Nullable
    static public Locale getPreferenceLocale ( Context context )
    {
        SharedPreferences preferences = context.getSharedPreferences(context.getString( R.string.preference_language ),Context.MODE_PRIVATE);
        String language = preferences.getString( context.getString( R.string.preference_language ), null );

        if ( language != null )
        {
            return new Locale(language);
        }

        return null;
    }
}
