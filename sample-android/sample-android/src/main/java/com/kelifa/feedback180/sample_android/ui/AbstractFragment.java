package com.kelifa.feedback180.sample_android.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by Romain Kelifa on 24/05/2016.
 */
abstract public class AbstractFragment extends Fragment implements OnFragmentCreatedListener, Reloadable
{
    protected OnFragmentCreatedListener listener;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        createUI();
        updateUI();
    }

    public void onFragmentCreated (@Nullable Fragment fragment)
    {
        if ( listener != null ) listener.onFragmentCreated(fragment);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }
}
