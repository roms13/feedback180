package com.kelifa.feedback180.sample_android.ui;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.kelifa.feedback180.sample_android.util.FontCache;

/**
 * Created by Romain Kelifa on 29/05/2016.
 * Based on the implementation of Norman Peitek
 * https://futurestud.io/blog/custom-fonts-on-android-using-font-styles
 */
public class CustomFontTextView extends TextView {

    public CustomFontTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public CustomFontTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public CustomFontTextView(Context context) {
        super(context);
    }

    public void setTypeface(Typeface tf, int style) {
        if (style == Typeface.BOLD) {
            super.setTypeface(FontCache.getTypeFace(getContext(),
                    "fonts/Wonder-Bold.ttf"));
        } else if (style == Typeface.ITALIC) {
            super.setTypeface(FontCache.getTypeFace(getContext(),
                    "fonts/Wonder-Italic.ttf"));
        } else {
            super.setTypeface(FontCache.getTypeFace(getContext(),
                    "fonts/Wonder.ttf"));
        }
    }
}
