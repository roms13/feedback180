package com.kelifa.feedback180.sample_android.ui;

import com.kelifa.feedback180.sample_android.service.OnTaskCompletedListener;

/**
 * Created by Romain Kelifa on 26/05/2016.
 */
public interface OnListChangeListener extends OnEntrySelectedListener, OnTaskCompletedListener
{}
