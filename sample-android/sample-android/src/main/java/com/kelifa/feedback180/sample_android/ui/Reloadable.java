package com.kelifa.feedback180.sample_android.ui;

/**
 * Created by Romain Kelifa on 30/05/2016.
 */
public interface Reloadable {
    void createUI();
    void updateUI();
}
