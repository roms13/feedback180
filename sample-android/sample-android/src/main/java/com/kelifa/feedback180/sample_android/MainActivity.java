package com.kelifa.feedback180.sample_android;

import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.kelifa.feedback180.sample_android.conf.Conf;
import com.kelifa.feedback180.sample_android.dto.Entry;
import com.kelifa.feedback180.sample_android.receiver.OnConnectionChangedListener;
import com.kelifa.feedback180.sample_android.receiver.WifiReceiver;
import com.kelifa.feedback180.sample_android.ui.DetailFragment;
import com.kelifa.feedback180.sample_android.ui.ListFragment;
import com.kelifa.feedback180.sample_android.ui.LocationFragment;
import com.kelifa.feedback180.sample_android.ui.OnListChangeListener;
import com.kelifa.feedback180.sample_android.util.Localization;

public class MainActivity extends AbstractMapActivity
        implements OnConnectionChangedListener, OnListChangeListener, View.OnClickListener
{
    static private String ACTIVITY_WAS_DISPLAYING_MAP = "ACTIVITY_WAS_DISPLAYING_MAP";
    static private String ACTIVITY_WAS_ONLINE = "ACTIVITY_WAS_ONLINE";

    @Nullable Boolean isOnline = null;

    private Button locationButton;

    private WifiReceiver receiver;
    boolean registered;

    @Override
    protected void inflateUI()
    {
        setContentView( R.layout.activity_main );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        receiver = new WifiReceiver(this);

        if ( savedInstanceState != null )
        {
            isOnline = savedInstanceState.getBoolean( ACTIVITY_WAS_ONLINE );

            FragmentManager manager = getSupportFragmentManager();
            LocationFragment fragment = (LocationFragment) manager.findFragmentByTag(getResources().getString(R.string.tag_map));
            if ( fragment != null && isDisplayingMap )
            {
                fragment.getMapAsync(this);
                setIsDisplayingMap( savedInstanceState.getBoolean( ACTIVITY_WAS_DISPLAYING_MAP ) );
                return;
            }
        }

        setIsDisplayingMap(false);
    }

    @Override
    public void createUI()
    {
        super.createUI();
        locationButton = (Button) findViewById(R.id.locationButton);
    }

    @Override
    public void updateUI()
    {
        super.updateUI();
        
        updateLocationButton();

        FragmentManager manager = getSupportFragmentManager();
        ListFragment listing = (ListFragment) manager.findFragmentByTag(getResources().getString(R.string.tag_listing));
        if ( listing != null ) listing.updateUI();

        DetailFragment detail = (DetailFragment) manager.findFragmentByTag(getResources().getString(R.string.tag_detail));
        if ( detail != null ) detail.updateUI();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        if ( registered )
        {
            unregisterReceiver( receiver );
            registered = false;
        }

        if ( locationButton != null )
        {
            locationButton.setOnClickListener(null);
        }
    }

    @Override
    protected void onResume()
    {
        super.onResume();
        if ( !registered )
        {
            IntentFilter filter = new IntentFilter( ConnectivityManager.CONNECTIVITY_ACTION );
            registerReceiver( receiver, filter );
            registered = true;
        }


        if ( locationButton != null )
        {
            locationButton.setOnClickListener(this);
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        outState.putBoolean( ACTIVITY_WAS_DISPLAYING_MAP, isDisplayingMap );
        outState.putBoolean( ACTIVITY_WAS_ONLINE,
                isOnline != null
                        ? isOnline
                        : WifiReceiver.hasWifi(this) && WifiReceiver.isConnected(this) );
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onTaskCompleted()
    {
        super.onTaskCompleted();
        updateLocationButton();
    }

    @Override
    public void onEntrySelected(int position)
    {
        Entry[] entries = model.getEntries();
        Entry entry = (Entry) entries[position];

        if ( isLargeScreen() )
        {
            FragmentManager manager = getSupportFragmentManager();
            LocationFragment locationFragment = (LocationFragment) manager.findFragmentByTag(getResources().getString(R.string.tag_map));

            DetailFragment detailFragment = DetailFragment.newInstance(entry);

            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

            if ( locationFragment != null )
            {
                transaction.detach( locationFragment );
            }

            // Add the fragment to the placeholder FrameLayout
            transaction.replace(R.id.fragmentDetailLayout, detailFragment, getResources().getString(R.string.tag_detail));

            transaction.commit();

            setIsDisplayingMap(false);
        }
        else
        {
            Intent intent = new Intent(this, DetailActivity.class);
            intent.putExtra(DetailFragment.EXTRA_MESSAGE_ENTRY, entry);
            startActivity(intent);
        }
    }

    public void onClick (View view)
    {
        if ( isLargeScreen() )
        {
            FragmentManager manager = getSupportFragmentManager();
            LocationFragment locationFragment = (LocationFragment) manager.findFragmentByTag(getResources().getString(R.string.tag_map));
            DetailFragment detailFragment = (DetailFragment) manager.findFragmentByTag(getResources().getString(R.string.tag_detail));
            final Boolean alreadyDetached = ( locationFragment != null );

            // Create a new Fragment to be placed in the activity layout
            if ( locationFragment == null ) locationFragment = LocationFragment.newInstance( null );

            FragmentTransaction transaction = manager.beginTransaction();

            if ( alreadyDetached )
            {
                if ( detailFragment != null ) transaction.remove(detailFragment);
                transaction.attach(locationFragment);
            }
            else
            {
                // Add the fragment to the placeholder FrameLayout
                transaction.replace(R.id.fragmentDetailLayout, locationFragment, getResources().getString(R.string.tag_map));
            }

            transaction.commit();

            locationFragment.getMapAsync(this);

            setIsDisplayingMap(true);
        }
        else
        {
            Intent intent = new Intent(this, LocationActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onIsDisplayingMapChanged()
    {
        super.onIsDisplayingMapChanged();
        updateLocationButton();
    }

    private boolean isLocationButtonEnabled ()
    {
        return ( hasEntries() && !isProcessing() && !isDisplayingMap );
    }

    private void updateLocationButton()
    {
        locationButton = (Button) findViewById(R.id.locationButton);
        locationButton.setEnabled(isLocationButtonEnabled());
        locationButton.setText(getString(R.string.display_map));
        locationButton.setTextSize(TypedValue.COMPLEX_UNIT_SP,
                Localization.isThai( getApplicationContext() ) ? Conf.THAI_FONT_SIZE : Conf.STANDARD_FONT_SIZE );
    }

    private boolean isProcessing()
    {
        FragmentManager manager = getSupportFragmentManager();
        ListFragment fragment = (ListFragment) manager.findFragmentByTag(getResources().getString(R.string.tag_listing));
        return ( fragment == null ) || fragment.isProcessing();
    }

    public void onConnectionChanged ()
    {
        if ( receiver != null )
        {
            FragmentManager manager = getSupportFragmentManager();
            ListFragment fragment = (ListFragment) manager.findFragmentByTag(getResources().getString(R.string.tag_listing));

            // current internet status
            boolean online = WifiReceiver.hasWifi(this) && WifiReceiver.isConnected(this);
            // if disconnected while processing
            boolean interrupted = !online && fragment.isProcessing();
            // if connected and fragment is idle
            boolean resumed = online && !fragment.contains() && !fragment.isProcessing();
            // first time we enter the activity
            boolean initialized = isOnline == null;
            // activity running but internet status just switched
            boolean switched = !initialized && ( online != isOnline );

            if ( initialized || switched )
            {
                if ( interrupted )
                {
                    fragment.cancel();
                }
                else if ( resumed )
                {
                    fragment.resume();
                }
            }

            if ( switched )
            {
                String message = online
                        ? getResources().getString(R.string.connectivity_back)
                        : getResources().getString(R.string.connectivity_gone);

                Toast.makeText(this,message,Toast.LENGTH_LONG).show();
            }

            // store current internet status
            isOnline = online;
        }
    }
}
