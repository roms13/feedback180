package com.kelifa.feedback180.sample_android.dto;

import android.os.Parcel;
import android.os.Parcelable;
import android.provider.BaseColumns;

import java.util.Date;

/**
 * Created by Romain Kelifa on 22/05/2016.
 */
public class Comment implements Parcelable, Provided, BaseColumns {
    static public String ID = "_id";
    static public String POST = "post";
    static public String DATE = "date";
    static public String ENTRY_ID = "entry_id";

    private int _id;
    private Date date;
    private String post;
    private int entry_id;

    public int getId() {
        return _id;
    }

    public void setId(int id) {
        this._id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getPost() {
        return post;
    }

    public void setPost(String post) {
        this.post = post;
    }

    public int getEntry_id() {
        return entry_id;
    }

    public void setEntry_id(int entry_id) {
        this.entry_id = entry_id;
    }

    protected Comment(Parcel in) {
        this._id = in.readInt();
        this.date = (Date) in.readSerializable();
        this.post = in.readString();
        this.entry_id = in.readInt();
    }

    public static final Creator<Comment> CREATOR = new Creator<Comment>() {
        @Override
        public Comment createFromParcel(Parcel in) {
            return new Comment(in);
        }

        @Override
        public Comment[] newArray(int size) {
            return new Comment[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this._id);
        dest.writeSerializable(this.date);
        dest.writeString(this.post);
        dest.writeInt(this.entry_id);
    }
}
