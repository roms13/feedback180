package com.kelifa.feedback180.sample_android.service;

/**
 * Created by Romain Kelifa on 15/05/2016.
 */
public interface OnTaskCompletedListener {
    void onTaskCompleted();
}
