package com.kelifa.feedback180.sample_android.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.kelifa.feedback180.sample_android.conf.Conf;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

public class EntriesTask extends AsyncTask<String,Void,String>
{
    private HttpURLConnection connection;
    private Context context;
    private int code;
    public String response;

    @Override
    protected String doInBackground(String... params)
    {
        try {
            retrieve();
        }
        catch ( IOException exception ) {
            this.response = exception.getMessage();
        }

        return this.response;
    }

    @Override
    protected void onPreExecute ()
    {
        response = null;
    }

    public String retrieve () throws IOException {
        InputStream stream = null;
        try {
            URL url = new URL("http://test.layer-net.com/api/sample.php");
            connection = (HttpURLConnection) url.openConnection();
            connection.setReadTimeout(10000 /* milliseconds */);
            connection.setConnectTimeout(15000 /* milliseconds */);
            connection.setRequestMethod("GET");
            connection.setDoInput(true);
            // Starts the query
            connection.connect();
            code = connection.getResponseCode();
            Log.d(Conf.HTTP_TAG, "The code is: " + code);
            stream = connection.getInputStream();

            // Convert the InputStream into a string
            response = convert(stream);
            return response;

            // Makes sure that the InputStream is closed after the app is
            // finished using it.
        } finally {
            if (stream != null) {
                stream.close();
                post();
            }
        }
    }

    // Reads an InputStream and converts it to a String.
    public String convert(InputStream stream) throws IOException, UnsupportedEncodingException {
        Reader reader = null;
        reader = new InputStreamReader(stream, Charset.forName("UTF-8"));
        StringBuilder sb = new StringBuilder();
        int count;
        while ((count = reader.read()) != -1) {
            sb.append((char) count);
        }
        return sb.toString();
    }

    private OnTaskCompletedListener listener;

    public EntriesTask(OnTaskCompletedListener listener){
        this.listener=listener;
    }

    // required methods
    protected void post()
    {
        listener.onTaskCompleted();
    }

    public void clean ()
    {
        listener = null;
    }
}