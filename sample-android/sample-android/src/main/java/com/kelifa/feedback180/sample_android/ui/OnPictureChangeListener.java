package com.kelifa.feedback180.sample_android.ui;

import com.squareup.picasso.Callback;

/**
 * Created by Romain Kelifa on 27/05/2016.
 */
public interface OnPictureChangeListener extends Callback, OnFragmentCreatedListener
{
}
