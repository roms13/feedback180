package com.kelifa.feedback180.sample_android.receiver;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;

import java.io.IOException;

/**
 * Created by Romain Kelifa on 26/05/2016.
 */
public class WifiReceiver extends BroadcastReceiver
{
    private OnConnectionChangedListener listener;

    public WifiReceiver (@Nullable OnConnectionChangedListener listener)
    {
        this.listener = listener;
    }

    @Override
    public void onReceive(Context context, Intent intent)
    {
        if ( listener != null )
        {
            listener.onConnectionChanged();
        }
    }

    @Override
    protected void finalize() throws Throwable
    {
        super.finalize();
        listener = null;
    }

    static public boolean hasWifi (Context context )
    {
        if ( context != null )
        {
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = manager.getActiveNetworkInfo();
            return ( networkInfo != null ) && ( networkInfo.getType() == ConnectivityManager.TYPE_WIFI );
        }

        return false;
    }

    static public boolean isConnected ( Context context )
    {
        if ( context != null )
        {
            ConnectivityManager manager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo networkInfo = manager.getActiveNetworkInfo();
            return ( networkInfo != null ) && networkInfo.isConnected();
        }

        return false;
    }

    static public boolean isOnline()
    {
        Runtime runtime = Runtime.getRuntime();
        try {

            Process ipProcess = runtime.exec("/system/bin/ping -c 1 8.8.8.8");
            int     exitValue = ipProcess.waitFor();
            return (exitValue == 0);

        } catch (IOException e)          { e.printStackTrace(); }
        catch (InterruptedException e) { e.printStackTrace(); }

        return false;
    }
};
