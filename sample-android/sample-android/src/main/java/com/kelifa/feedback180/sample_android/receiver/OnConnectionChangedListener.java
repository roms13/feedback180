package com.kelifa.feedback180.sample_android.receiver;

/**
 * Created by Romain Kelifa on 27/05/2016.
 */
public interface OnConnectionChangedListener {
    void onConnectionChanged ();
}
