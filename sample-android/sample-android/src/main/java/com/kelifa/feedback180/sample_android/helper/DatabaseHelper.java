package com.kelifa.feedback180.sample_android.helper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;
import android.util.Log;

import com.kelifa.feedback180.sample_android.conf.Conf;
import com.kelifa.feedback180.sample_android.dto.Comment;
import com.kelifa.feedback180.sample_android.dto.Entry;

/**
 * Created by Romain Kelifa on 25/05/2016.
 */
public class DatabaseHelper extends SQLiteOpenHelper
{
    static public String DATABASE_NAME = "sample-android.db";
    static public int DATABASE_VERSION = 1;
    static public String TABLE_NAME = "comment";

    public DatabaseHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        Log.d(Conf.INITIALIZATION_TAG,"Creating database for version " + DATABASE_VERSION + "...");

        db.execSQL("CREATE TABLE " + TABLE_NAME
                + " ("
                + Comment.ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + Comment.POST + " TEXT,"
                + Comment.DATE + " DATETIME,"
                + Comment.ENTRY_ID + " INTEGER"
                + ");");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {
        Log.d(Conf.INITIALIZATION_TAG,"Upgrading database from version " + oldVersion + " to version " + newVersion + "...");

        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME + ";");
        onCreate(db);
    }

    public Cursor getCommentById ( String id, String[] projection, String selection, String[] selectionArgs, String sortOrder )
    {
        if ( id == null || Integer.parseInt(id) < 1 )
        {
            return getComments(projection,selection,selectionArgs,sortOrder);
        }

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(TABLE_NAME);
        builder.appendWhere(Comment.ID + " = " + id);
        Cursor cursor = builder.query(getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        return cursor;
    }

    public Cursor getCommentsByEntryId ( String id, String[] projection, String selection, String[] selectionArgs, String sortOrder )
    {
        if ( id == null || Integer.parseInt(id) < 1 )
        {
            return getComments(projection,selection,selectionArgs,sortOrder);
        }

        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(TABLE_NAME);
        builder.appendWhere(Comment.ENTRY_ID + " = " + id);
        Cursor cursor = builder.query(getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        return cursor;
    }

    public Cursor getComments(Entry entry, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        String id = entry != null ? Integer.toString( entry.getId() ) : Integer.toString( -1 );
        return getCommentsByEntryId(id,projection,selection,selectionArgs,sortOrder);
    }

    public Cursor getComments(Comment comment, String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        String id = comment != null ? Integer.toString( comment.getId() ) : Integer.toString( -1 );
        return getCommentById(id,projection,selection,selectionArgs,sortOrder);
    }

    public Cursor getComments(String[] projection, String selection, String[] selectionArgs, String sortOrder)
    {
        SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
        builder.setTables(TABLE_NAME);
        Cursor cursor = builder.query(getReadableDatabase(),
                projection,
                selection,
                selectionArgs,
                null,
                null,
                sortOrder);

        return cursor;
    }

    public long insertComment ( ContentValues values ) throws SQLException
    {
        long id = getWritableDatabase().insert(TABLE_NAME, "", values);

        if( id < 1 )
        {
            throw new SQLException("Failed to add an image");
        }

        return id;
    }
}
