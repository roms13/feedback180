package com.kelifa.feedback180.sample_android.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.kelifa.feedback180.sample_android.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Romain Kelifa on 18/05/2016.
 */
public class PictureFragment extends AbstractFragment implements Callback
{
    private LoadingFragment loadingFragment;
    private FrameLayout loadingLayout;
    public ImageView imageView;
    private View view;

    private String path;
    private boolean loading;

    public static PictureFragment newInstance(OnPictureChangeListener listener) {
        PictureFragment fragment = new PictureFragment();
        fragment.listener = listener;
        return fragment;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_picture, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        onFragmentCreated(this);
    }

    @Override
    public void onFragmentCreated(@Nullable Fragment fragment)
    {
        super.onFragmentCreated(fragment);

        if ( !loading )
        {
            load();
        }
    }

    @Override
    public void createUI()
    {
        loadingFragment = new LoadingFragment();
        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.fragmentLoadingPictureLayout, loadingFragment).commit();

        imageView = (ImageView) view.findViewById(R.id.fragmentPictureImageView) ;
        loadingLayout = (FrameLayout) view.findViewById(R.id.fragmentLoadingPictureLayout);
    }

    @Override
    public void updateUI()
    {
        if ( loadingFragment != null ) loadingFragment.setNoneMessage( getString( R.string.picture_task_none ) );
        updateLoading();
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    @Override
    public void onSuccess()
    {
        setLoading( false );

        if ( getListener() != null ) getListener().onSuccess();
    }

    @Override
    public void onError()
    {
        setLoading( false );
        loadingLayout.setVisibility( View.VISIBLE );

        if ( getListener() != null ) getListener().onError();
    }

    private void load ()
    {
        if ( !loading )
        {
            if ( path != null )
            {
                URL uri = null;

                try
                {
                    uri = new URL( path );
                }
                catch ( MalformedURLException e )
                {
                    e.printStackTrace();
                }

                if ( uri != null )
                {
                    setLoading( true );

                    Picasso.with( getActivity() )
                            .load( uri.toString() )
                            .into( imageView, this
                            );
                }
            }
        }
    }

    private void resume ()
    {
        if ( isAdded() )
        {
            if ( loading )
            {
                cancel();
            }

            load();
        }
    }

    private void cancel ()
    {
        if ( loading )
        {
            Picasso.with( getActivity() )
                    .cancelRequest( imageView );

            setLoading( false );
        }
    }

    private void updateLoading()
    {
        if ( isAdded() )
        {
            if ( loadingFragment != null )
            {
                loadingFragment.setProcessing( this.loading );
            }

            if ( loadingLayout != null )
            {
                loadingLayout.setVisibility( loading ? View.VISIBLE : View.GONE );
            }
        }
    }

    private void setLoading ( boolean loading )
    {
        if ( this.loading != loading )
        {
            this.loading = loading;
            updateLoading();
        }
    }

    public String getPath()
    {
        return path;
    }

    public void setPath(String path)
    {
        if ( this.path == null || !this.path.equals(path) )
        {
            this.path = path;
            resume();
        }
    }

    private OnPictureChangeListener getListener ()
    {
        return (OnPictureChangeListener) listener;
    }
}
