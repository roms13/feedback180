package com.kelifa.feedback180.sample_android.ui;

import android.content.ContentValues;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kelifa.feedback180.sample_android.R;
import com.kelifa.feedback180.sample_android.conf.Conf;
import com.kelifa.feedback180.sample_android.dto.Comment;
import com.kelifa.feedback180.sample_android.dto.Entry;
import com.kelifa.feedback180.sample_android.provider.CommentProvider;
import com.kelifa.feedback180.sample_android.util.Localization;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Romain Kelifa on 22/05/2016.
 */
public class CommentFragment extends AbstractFragment
        implements LoaderManager.LoaderCallbacks<Cursor>, View.OnClickListener
{
    final static public String EXTRA_MESSAGE_COMMENT = "com.kelifa.feedback180.sample-android.comment.MESSAGE";
    final private static int COMMENT_LOADER = 0;

    private View view;
    private EditText commentEditText;
    private RecyclerView commentListView;
    private Button commentSubmitButton;
    private TextView commentTitleText;

    private CommentAdapter adapter;
    private Entry entry;

    static public CommentFragment newInstance (OnFragmentCreatedListener listener,
                                               Entry entry)
    {
        CommentFragment fragment = new CommentFragment();
        fragment.listener = listener;
        Bundle args = new Bundle();
        args.putParcelable( EXTRA_MESSAGE_COMMENT, entry);
        fragment.setArguments(args);
        return fragment;
    }

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_comment, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        onFragmentCreated(this);
    }

    @Override
    public void createUI()
    {
        commentEditText = (EditText) view.findViewById(R.id.commentEditText);
        commentListView = (RecyclerView) view.findViewById(R.id.commentRecyclerView);
        commentSubmitButton = (Button) view.findViewById(R.id.commentSubmitButton);
        commentTitleText = (TextView) view.findViewById(R.id.commentTitleText);

        commentListView.setHasFixedSize(false);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        commentListView.setLayoutManager(layoutManager);

        adapter = new CommentAdapter(null,R.layout.adapter_comment);
        commentListView.setAdapter(adapter);
    }

    @Override
    public void updateUI()
    {
        if ( isAdded() )
        {
            commentTitleText = (TextView) view.findViewById(R.id.commentTitleText);
            commentEditText = (EditText) view.findViewById(R.id.commentEditText);
            commentSubmitButton = (Button) view.findViewById(R.id.commentSubmitButton);

            commentTitleText.setText(getString(R.string.comment_title));
            commentTitleText.setTextSize(TypedValue.COMPLEX_UNIT_SP,
                    Localization.isThai( getContext() ) ? Conf.THAI_FONT_SIZE : Conf.STANDARD_FONT_SIZE );
            commentEditText.setHint(getString(R.string.comment_hint));
            commentSubmitButton.setText(getString(R.string.comment_submit));
        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        commentSubmitButton.setOnClickListener(null);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        commentSubmitButton.setOnClickListener(this);
    }

    @Override
    public void onDetach()
    {
        super.onDetach();
        listener = null;
    }

    public void refresh ()
    {
        FragmentActivity activity = getActivity();
        LoaderManager manager = activity != null ? activity.getSupportLoaderManager() : null;
        Loader loader = manager != null ? manager.getLoader(COMMENT_LOADER) : null;

        if ( loader != null )
            getActivity().getSupportLoaderManager().restartLoader(COMMENT_LOADER, getArguments(), this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        Uri uri = CommentProvider.CONTENT_URI;
        entry = (args != null) && (args.containsKey(EXTRA_MESSAGE_COMMENT))
                ? (Entry) args.getParcelable(EXTRA_MESSAGE_COMMENT)
                : null;
        if ( entry != null )
        {
            uri = Uri.withAppendedPath(uri,"entry/"+entry.getId());
        }

        switch (id) {
            case COMMENT_LOADER:
                // Returns a new CursorLoader
                return new CursorLoader(
                        getActivity(),                  // Parent activity context
                        uri,                            // Uri to query
                        new String[]{Comment.ID,Comment.POST,Comment.DATE,Comment.ENTRY_ID}, // Projection to return
                        null,                           // Entry joint
                        null,                           // No selection arguments
                        null                            // Default sort order
                );
            default:
                // An invalid id was passed in
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data)
    {
        adapter.swapCursor(data);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adapter.swapCursor(null);
    }

    @Override
    public void onFragmentCreated( Fragment fragment )
    {
        getActivity().getSupportLoaderManager().initLoader(COMMENT_LOADER, getArguments(), this);
        super.onFragmentCreated(fragment);
    }

    @Override
    public void onClick(View v)
    {
        if ( commentEditText.getText().toString().equals("") )
        {
            Toast.makeText(getContext(),getString(R.string.comment_empty),Toast.LENGTH_LONG).show();
            return;
        }
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aaa Z",getResources().getConfiguration().locale);
        String formattedDate = df.format(c.getTime());
        ContentValues values = new ContentValues();
        entry = null;

        Bundle args = getArguments();
        if (args != null)
        {
            if (args.containsKey(EXTRA_MESSAGE_COMMENT))
            {
                entry = args.getParcelable(EXTRA_MESSAGE_COMMENT);

                if ( entry != null )
                {
                    values.put(Comment.POST, commentEditText.getText().toString());
                    values.put(Comment.DATE, formattedDate);
                    values.put(Comment.ENTRY_ID, entry.getId());

                    getActivity().getContentResolver().insert(CommentProvider.CONTENT_URI, values);
                    commentEditText.setText(null);
                    refresh();
                }
            }
        }
    }
}
