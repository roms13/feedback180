package com.kelifa.feedback180.sample_android.dto;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.Gson;

/**
 * Created by Romain Kelifa on 17/05/2016.
 */
public class Entry implements Parcelable, Provided {
    private int id;
    private int category_id;
    private int station_id;
    private String name;
    private String detail;
    private String location;
    private String condition;
    private String pic;
    private String pic_thumb;
    private Float lat;
    private Float lng;

    public Entry ( Parcel in )
    {
        this.id = in.readInt();
        this.category_id = in.readInt();
        this.station_id = in.readInt();
        this.name = in.readString();
        this.detail = in.readString();
        this.location = in.readString();
        this.condition = in.readString();
        this.pic = in.readString();
        this.pic_thumb = in.readString();
        this.lat = in.readFloat();
        this.lng = in.readFloat();
    }

    public static Entry fromJson(String s) {
        return new Gson().fromJson(s, Entry.class);
    }
    public String toString() {
        return new Gson().toJson(this);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public int getStation_id() {
        return station_id;
    }

    public void setStation_id(int station_id) {
        this.station_id = station_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getPic() {
        return pic;
    }

    public void setPic(String pic) {
        this.pic = pic;
    }

    public String getPic_thumb() {
        return pic_thumb;
    }

    public void setPic_thumb(String pic_thumb) {
        this.pic_thumb = pic_thumb;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(category_id);
        dest.writeInt(station_id);
        dest.writeString(name);
        dest.writeString(detail);
        dest.writeString(location);
        dest.writeString(condition);
        dest.writeString(pic);
        dest.writeString(pic_thumb);
        dest.writeFloat(lat);
        dest.writeFloat(lng);
    }

    public static final Parcelable.Creator<Entry> CREATOR = new Parcelable.Creator<Entry>()
    {
        @Override
        public Entry createFromParcel(Parcel source)
        {
            return new Entry(source);
        }

        @Override
        public Entry[] newArray(int size)
        {
            return new Entry[size];
        }
    };
}
