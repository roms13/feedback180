package com.kelifa.feedback180.sample_android.conf;

/**
 * Created by Romain Kelifa on 18/05/2016.
 */
public class Conf {
    final static public String HTTP_TAG = "Http";
    final static public String PICASSO_TAG = "Picasso";
    final static public String DATA_TAG = "Data";
    final static public String CONNECTIVITY_TAG = "Connectivity";
    final static public String PROCESSING_TAG = "Processing";
    final static public String PROVIDER_TAG = "CommentProvider";
    final static public String INITIALIZATION_TAG = "Initializing";

    final static public float THAI_FONT_SIZE = 21;
    final static public float STANDARD_FONT_SIZE = 16;
}
