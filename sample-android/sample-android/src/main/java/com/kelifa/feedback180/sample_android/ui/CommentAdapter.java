package com.kelifa.feedback180.sample_android.ui;

import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kelifa.feedback180.sample_android.dto.Comment;

/**
 * Created by Romain Kelifa on 26/05/2016.
 */
public class CommentAdapter extends CursorRecyclerAdapter<CommentViewHolder>
{
    private Cursor cursor;
    private int layout;

    public CommentAdapter(Cursor cursor, int layout)
    {
        super(cursor);
        this.cursor = cursor;
        this.layout = layout;
    }

    @Override
    public CommentViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        return new CommentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentViewHolder holder, Cursor cursor)
    {
        String date = cursor.getString(cursor.getColumnIndex(Comment.DATE));
        String post = cursor.getString(cursor.getColumnIndex(Comment.POST));

        holder.commentDateTextView.setText(date);
        holder.commentPostTextView.setText(post);
    }
}
