package com.kelifa.feedback180.sample_android.ui;

import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kelifa.feedback180.sample_android.R;
import com.kelifa.feedback180.sample_android.receiver.OnConnectionChangedListener;
import com.kelifa.feedback180.sample_android.receiver.WifiReceiver;

/**
 * Created by Romain Kelifa on 18/05/2016.
 */
public class LoadingFragment extends AbstractFragment implements OnConnectionChangedListener
{
    static final private int DEFAULT_MESSAGE_ERROR = R.string.default_task_error;
    static final private int DEFAULT_MESSAGE_LOADING = R.string.default_task_loading;
    static final private int DEFAULT_MESSAGE_NONE = R.string.default_task_none;
    private String errorMessage;
    private String loadingMessage;
    private String noneMessage;

    private View view;
    private TextView textView;
    private ImageView iconView;
    private WifiReceiver receiver;
    private boolean processing;

    @Override public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.fragment_loading, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        receiver = new WifiReceiver(this);
    }

    @Override
    public void createUI()
    {
        textView = (TextView) view.findViewById( R.id.fragmentLoadingTextView);
        iconView = (ImageView) view.findViewById( R.id.fragmentLoadingImageView);
    }

    @Override
    public void updateUI()
    {
        updateText();
        updateIcon();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        getActivity().unregisterReceiver(receiver);
    }

    @Override
    public void onResume()
    {
        super.onResume();
        getActivity().registerReceiver(receiver,new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    private void updateText ()
    {
        if ( isAdded() )
        {
            if ( textView != null )
            {
                // current internet status
                boolean online = WifiReceiver.hasWifi(getContext()) && WifiReceiver.isConnected(getContext());

                if ( online )
                {
                    if ( processing ) textView.setText( getLoadingMessage() );
                    else textView.setText( getNoneMessage() );
                }
                else
                {
                    textView.setText( getString( R.string.connectivity_none ) );
                }
            }
        }
    }

    private void updateIcon ()
    {
        if ( isAdded() )
        {
            // current internet status
            boolean online = WifiReceiver.hasWifi(getContext()) && WifiReceiver.isConnected(getContext());
            if ( online )
            {
                if ( processing ) iconView.setImageResource( R.drawable.an_loading );
                else iconView.setImageResource( R.drawable.ic_none);
            }
            else
            {
                iconView.setImageResource( R.drawable.ic_offline );
            }
        }
    }

    public void onConnectionChanged ()
    {
        updateUI();
    }

    public void setProcessing ( boolean processing )
    {
        if ( this.processing != processing )
        {
            this.processing = processing;
            updateUI();
        }
    }

    private String getErrorMessage ()
    {
        if ( errorMessage != null )
            return errorMessage;

        return getString( DEFAULT_MESSAGE_ERROR );
    }

    public void setErrorMessage ( String errorMessage )
    {
        if ( this.errorMessage == null || !this.errorMessage.equals( errorMessage ) )
        {
            this.errorMessage = errorMessage;
            updateUI();
        }
    }

    private String getLoadingMessage ()
    {
        if ( loadingMessage != null )
                return loadingMessage;

        return getString( DEFAULT_MESSAGE_LOADING );
    }

    public void setLoadingMessage ( String loadingMessage )
    {
        if ( this.loadingMessage == null || !this.loadingMessage.equals(loadingMessage) )
        {
            this.loadingMessage = loadingMessage;
            updateUI();
        }
    }

    private String getNoneMessage ()
    {
        if ( noneMessage != null )
                return noneMessage;

        return getString( DEFAULT_MESSAGE_NONE );
    }

    public void setNoneMessage ( String noneMessage )
    {
        if ( this.noneMessage == null || !this.noneMessage.equals(noneMessage) )
        {
            this.noneMessage = noneMessage;
            updateUI();
        }
    }
}
