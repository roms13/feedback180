package com.kelifa.feedback180.sample_android;

import android.content.Intent;
import android.os.Bundle;

import com.kelifa.feedback180.sample_android.dto.Entry;
import com.kelifa.feedback180.sample_android.ui.DetailFragment;

/**
 * Created by Romain Kelifa on 18/05/2016.
 */
public class DetailActivity extends AbstractActivity
{
    DetailFragment fragment;
    private Entry entry;

    @Override
    protected void extractExtras()
    {
        super.extractExtras();
        Intent intent = getIntent();
        entry = intent.getExtras().getParcelable( DetailFragment.EXTRA_MESSAGE_ENTRY);
    }

    @Override
    protected void inflateUI()
    {
        super.inflateUI();
        setContentView(R.layout.activity_detail);
    }

    @Override
    public void createUI()
    {
        super.createUI();

        if ( entry != null )
        {
            int id = R.id.fragmentLayout;
            fragment = DetailFragment.newInstance(entry);

            getSupportFragmentManager().beginTransaction()
                    .replace(id, fragment)
                    .commit();
        }
    }

    @Override
    public void updateUI()
    {
        super.updateUI();
        if ( fragment != null )
        {
            fragment.updateUI();
        }
    }
}
