package com.kelifa.feedback180.sample_android.ui;

import android.os.Bundle;

import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.SupportMapFragment;

/**
 * Created by Romain Kelifa on 26/05/2016.
 */
public class LocationFragment extends SupportMapFragment
{
    public static LocationFragment newInstance(GoogleMapOptions options)
    {
        LocationFragment fragment = new LocationFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable("MapOptions", options);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle bundle)
    {
        super.onCreate(bundle);
        setRetainInstance(true);
    }
}
